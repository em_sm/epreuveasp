--CREATE DATABASE [HolidayRental];
--GO
use HolidayRental;
go
CREATE TABLE [dbo].[Avis](
	[idAvis] [int] IDENTITY(1,1) NOT NULL,
	[dateAvis] [date] NULL,
	[scoreAvis] [float] NULL,
	[DescriptionAvis] [varchar](400) NULL,
	[fk_Membre] [int] NULL,
	[fk_Bien] [int] NULL,
 CONSTRAINT [PK_Avis] PRIMARY KEY CLUSTERED 
(
	[idAvis] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AvisMembreBien]    Script Date: 20-02-20 09:23:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AvisMembreBien](
	[idAvis] [int] IDENTITY(1,1) NOT NULL,
	[note] [int] NOT NULL,
	[message] [ntext] NOT NULL,
	[idMembre] [int] NOT NULL,
	[idBien] [int] NOT NULL,
	[DateAvis] [datetime] NOT NULL,
	[Approuve] [bit] NOT NULL,
 CONSTRAINT [PK_AvisMembreBien] PRIMARY KEY CLUSTERED 
(
	[idAvis] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Bien]    Script Date: 20-02-20 09:23:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Bien](
	[idBien] [int] IDENTITY(1,1) NOT NULL,
	[titre] [varchar](100) NOT NULL,
	[descriptionCourte] [varchar](50) NULL,
	[descriptionLongue] [varchar](500) NULL,
	[photo] [varchar](50) NOT NULL,
	[dateCreation] [date] NOT NULL,
	[dateSuppression] [date] NULL,
	[capacité] [varchar](50) NULL,
	[fk_Pays] [int] NULL,
	[fk_Membre] [int] NULL,
 CONSTRAINT [PK_Bien] PRIMARY KEY CLUSTERED 
(
	[idBien] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[BienEchange]    Script Date: 20-02-20 09:23:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BienEchange](
	[idBien] [int] IDENTITY(1,1) NOT NULL,
	[titre] [nvarchar](50) NOT NULL,
	[DescCourte] [nvarchar](150) NOT NULL,
	[DescLong] [ntext] NOT NULL,
	[NombrePerson] [int] NOT NULL,
	[Pays] [int] NOT NULL,
	[Ville] [nvarchar](50) NOT NULL,
	[Rue] [nvarchar](50) NOT NULL,
	[Numero] [nvarchar](5) NOT NULL,
	[CodePostal] [nvarchar](50) NOT NULL,
	[Photo] [nvarchar](50) NOT NULL,
	[AssuranceObligatoire] [bit] NOT NULL,
	[isEnabled] [bit] NOT NULL,
	[DisabledDate] [date] NULL,
	[Latitude] [nvarchar](50) NULL,
	[Longitude] [nvarchar](50) NULL,
	[idMembre] [int] NOT NULL,
	[DateCreation] [date] NOT NULL,
 CONSTRAINT [PK_BienEchange] PRIMARY KEY CLUSTERED 
(
	[idBien] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Caracteristique]    Script Date: 20-02-20 09:23:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Caracteristique](
	[idCaractèristique] [int] IDENTITY(1,1) NOT NULL,
	[libelle] [varchar](150) NULL,
	[typeBien] [varchar](150) NULL,
 CONSTRAINT [PK_Caracteristique] PRIMARY KEY CLUSTERED 
(
	[idCaractèristique] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CaracteristiqueBien]    Script Date: 20-02-20 09:23:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CaracteristiqueBien](
	[idCaracteristiqueBien] [int] IDENTITY(1,1) NOT NULL,
	[fk_idCaracteristique] [int] NULL,
	[fk_idBien] [int] NULL,
 CONSTRAINT [PK_CaracteristiqueBien] PRIMARY KEY CLUSTERED 
(
	[idCaracteristiqueBien] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Echange]    Script Date: 20-02-20 09:23:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Echange](
	[idEchange] [int] IDENTITY(1,1) NOT NULL,
	[dateReservation] [date] NOT NULL,
	[dateDemande] [date] NOT NULL,
	[dateDebut] [date] NOT NULL,
	[dateFin] [date] NOT NULL,
	[optionAssurence] [bit] NOT NULL,
	[valider] [bit] NOT NULL,
	[numEchange] [varchar](250) NOT NULL,
	[fk_Membre] [int] NULL,
	[fk_Bien] [int] NULL,
 CONSTRAINT [PK_Echange] PRIMARY KEY CLUSTERED 
(
	[idEchange] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[HistoAvis]    Script Date: 20-02-20 09:23:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HistoAvis](
	[idAvis] [int] NOT NULL,
	[DateAvis] [datetime] NOT NULL,
	[NoteAvis] [int] NOT NULL,
	[idMembre] [int] NOT NULL,
	[idProprio] [int] NOT NULL,
	[TitreBien] [nvarchar](50) NOT NULL,
	[VilleBien] [nvarchar](50) NOT NULL,
	[NbEchange] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[idAvis] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Membre]    Script Date: 20-02-20 09:23:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Membre](
	[idMembre] [int] IDENTITY(1,1) NOT NULL,
	[Nom] [nvarchar](50) NOT NULL,
	[Prenom] [nvarchar](50) NOT NULL,
	[Email] [nvarchar](256) NOT NULL,
	[Pays] [int] NOT NULL,
	[Telephone] [nvarchar](20) NOT NULL,
	[Login] [nvarchar](50) NOT NULL,
	[Password] [nvarchar](256) NOT NULL,
	[PhotoUser] [nchar](10) NULL,
	[isDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_membre] PRIMARY KEY CLUSTERED 
(
	[idMembre] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MembreBienEchange]    Script Date: 20-02-20 09:23:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MembreBienEchange](
	[idMembre] [int] NOT NULL,
	[idBien] [int] NOT NULL,
	[DateDebEchange] [date] NOT NULL,
	[DateFinEchange] [date] NOT NULL,
	[Assurance] [bit] NULL,
	[Valide] [bit] NOT NULL,
 CONSTRAINT [PK_MembreBienEchange] PRIMARY KEY CLUSTERED 
(
	[idMembre] ASC,
	[idBien] ASC,
	[DateDebEchange] ASC,
	[DateFinEchange] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Options]    Script Date: 20-02-20 09:23:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Options](
	[idOption] [int] IDENTITY(1,1) NOT NULL,
	[Libelle] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Options] PRIMARY KEY CLUSTERED 
(
	[idOption] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OptionsBien]    Script Date: 20-02-20 09:23:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OptionsBien](
	[idOption] [int] NOT NULL,
	[idBien] [int] NOT NULL,
	[Valeur] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_OptionsBien] PRIMARY KEY CLUSTERED 
(
	[idOption] ASC,
	[idBien] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Pays]    Script Date: 20-02-20 09:23:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Pays](
	[idPays] [int] IDENTITY(1,1) NOT NULL,
	[Libelle] [nvarchar](50) NOT NULL,
	[Photo] [nvarchar](50) NULL,
 CONSTRAINT [PK_Pays] PRIMARY KEY CLUSTERED 
(
	[idPays] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[User]    Script Date: 20-02-20 09:23:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[User](
	[idUser] [int] IDENTITY(1,1) NOT NULL,
	[login] [varchar](100) NOT NULL,
	[password] [varchar](10) NOT NULL,
 CONSTRAINT [PK_Users] PRIMARY KEY CLUSTERED 
(
	[idUser] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[AvisMembreBien] ON 
GO
INSERT [dbo].[AvisMembreBien] ([idAvis], [note], [message], [idMembre], [idBien], [DateAvis], [Approuve]) VALUES (2, 4, N'Beaucoup trop humide', 1, 2, CAST(N'2015-03-06T00:00:00.000' AS DateTime), 1)
GO
INSERT [dbo].[AvisMembreBien] ([idAvis], [note], [message], [idMembre], [idBien], [DateAvis], [Approuve]) VALUES (4, 10, N'Quel merveille', 4, 3, CAST(N'2015-03-06T00:00:00.000' AS DateTime), 1)
GO
INSERT [dbo].[AvisMembreBien] ([idAvis], [note], [message], [idMembre], [idBien], [DateAvis], [Approuve]) VALUES (5, 1, N'Vraiment n''importe quoi ce bien', 1, 2, CAST(N'2015-03-06T00:00:00.000' AS DateTime), 0)
GO
INSERT [dbo].[AvisMembreBien] ([idAvis], [note], [message], [idMembre], [idBien], [DateAvis], [Approuve]) VALUES (6, 2, N'yes', 7, 2, CAST(N'2016-03-19T23:51:58.603' AS DateTime), 0)
GO
INSERT [dbo].[AvisMembreBien] ([idAvis], [note], [message], [idMembre], [idBien], [DateAvis], [Approuve]) VALUES (7, 8, N'Très bien', 3, 2, CAST(N'2017-07-18T15:43:58.650' AS DateTime), 0)
GO
INSERT [dbo].[AvisMembreBien] ([idAvis], [note], [message], [idMembre], [idBien], [DateAvis], [Approuve]) VALUES (8, 8, N'Très bien', 3, 2, CAST(N'2017-07-18T15:44:55.703' AS DateTime), 0)
GO
SET IDENTITY_INSERT [dbo].[AvisMembreBien] OFF
GO
SET IDENTITY_INSERT [dbo].[BienEchange] ON 
GO
INSERT [dbo].[BienEchange] ([idBien], [titre], [DescCourte], [DescLong], [NombrePerson], [Pays], [Ville], [Rue], [Numero], [CodePostal], [Photo], [AssuranceObligatoire], [isEnabled], [DisabledDate], [Latitude], [Longitude], [idMembre], [DateCreation]) VALUES (2, N'Un peu Humide', N'Petite maison sous-marine tout confort', N'Maison tout confort située au large de Miami. <br /> Possibilité de venir avec votre animal de compagnie si celui-ci adore les longs séjours dans l''eau ou si il est naturellement amphibie. Pas de piscine mais une magnifique serre contenant algues et anémones.', 2, 6, N'Miami', N'UnderStreet', N'8', N'123456', N'maisonSousMarine.jpg', 0, 1, NULL, N'25.774', N'36.874', 1, CAST(N'2015-03-06' AS Date))
GO
INSERT [dbo].[BienEchange] ([idBien], [titre], [DescCourte], [DescLong], [NombrePerson], [Pays], [Ville], [Rue], [Numero], [CodePostal], [Photo], [AssuranceObligatoire], [isEnabled], [DisabledDate], [Latitude], [Longitude], [idMembre], [DateCreation]) VALUES (3, N'Vue imprenable sur le grand Canyon', N'Maison perchée sur la falaise offrant une vue imprenable.', N'Vivre comme un aigle et respirer l''air pur.<br > Cette maison est un petit paradis perché offrant confort moderne.', 1, 7, N'Colorado Sping', N'RockNRoll', N'10', N'784521', N'rockHouse.jpg', 1, 0, CAST(N'2017-02-02' AS Date), N'36.159420', N'-112.202579', 3, CAST(N'2015-03-06' AS Date))
GO
INSERT [dbo].[BienEchange] ([idBien], [titre], [DescCourte], [DescLong], [NombrePerson], [Pays], [Ville], [Rue], [Numero], [CodePostal], [Photo], [AssuranceObligatoire], [isEnabled], [DisabledDate], [Latitude], [Longitude], [idMembre], [DateCreation]) VALUES (18, N'La cabane au fond du canyon', N'Magnifique petite cabane de trappeur', N'Devenez le david Croquet du 21ème siècle', 1, 7, N'Colorado Sping', N'RockNRoll', N'10', N'784521', N'rockHouse.jpg', 1, 1, NULL, N'36.159420', N'-112.202579', 3, CAST(N'2015-03-06' AS Date))
GO
SET IDENTITY_INSERT [dbo].[BienEchange] OFF
GO
SET IDENTITY_INSERT [dbo].[Membre] ON 
GO
INSERT [dbo].[Membre] ([idMembre], [Nom], [Prenom], [Email], [Pays], [Telephone], [Login], [Password], [PhotoUser], [isDeleted]) VALUES (1, N'Pink', N'Panther', N'pink@panther.com', 7, N'555-456325', N'Pink@bbel.be', N'Pink', NULL, 0)
GO
INSERT [dbo].[Membre] ([idMembre], [Nom], [Prenom], [Email], [Pays], [Telephone], [Login], [Password], [PhotoUser], [isDeleted]) VALUES (3, N'Admin', N'Admin', N'admin@HomeShare.be', 1, N'4562325214', N'Admin', N'Admin', NULL, 0)
GO
INSERT [dbo].[Membre] ([idMembre], [Nom], [Prenom], [Email], [Pays], [Telephone], [Login], [Password], [PhotoUser], [isDeleted]) VALUES (4, N'Dolphin', N'Flipper', N'dolphin.Flip@sea.us', 1, N'0000000000', N'Dol', N'Phin', NULL, 0)
GO
INSERT [dbo].[Membre] ([idMembre], [Nom], [Prenom], [Email], [Pays], [Telephone], [Login], [Password], [PhotoUser], [isDeleted]) VALUES (5, N'Mike', N'Mike', N'mpg', 1, N'-5', N'mike', N'ZLTQ9HyTziPRV+aKWHZzVig9ybY8RZ1F0ODjmzpkubk=', NULL, 0)
GO
INSERT [dbo].[Membre] ([idMembre], [Nom], [Prenom], [Email], [Pays], [Telephone], [Login], [Password], [PhotoUser], [isDeleted]) VALUES (6, N'zorro', N'test', N'lk@dd.be', 7, N'-12', N'azerty', N'azerty', NULL, 0)
GO
INSERT [dbo].[Membre] ([idMembre], [Nom], [Prenom], [Email], [Pays], [Telephone], [Login], [Password], [PhotoUser], [isDeleted]) VALUES (7, N'MIke', N'mikel', N'mpg@sss.be', 1, N'-45', N'poi', N'+wcWYXmxzjxJ3/S1kMRNZhGWCL5APZvoxD5zcqmX7nU=', NULL, 0)
GO
INSERT [dbo].[Membre] ([idMembre], [Nom], [Prenom], [Email], [Pays], [Telephone], [Login], [Password], [PhotoUser], [isDeleted]) VALUES (9, N'm', N'm', N'm@dd.com', 1, N'-4', N'test', N'test1234=', NULL, 0)
GO
INSERT [dbo].[Membre] ([idMembre], [Nom], [Prenom], [Email], [Pays], [Telephone], [Login], [Password], [PhotoUser], [isDeleted]) VALUES (10, N'MIke', N'mikel', N'fd', 7, N'-12', N'ha', N'ha', NULL, 0)
GO
INSERT [dbo].[Membre] ([idMembre], [Nom], [Prenom], [Email], [Pays], [Telephone], [Login], [Password], [PhotoUser], [isDeleted]) VALUES (11, N'mikel', N'mikel', N'l@dd.com', 1, N'-45', N'po', N'YZmuzyOrp+h7La+4tJFSYNqF489TVoGXt+RRmCOS+44=', NULL, 0)
GO
INSERT [dbo].[Membre] ([idMembre], [Nom], [Prenom], [Email], [Pays], [Telephone], [Login], [Password], [PhotoUser], [isDeleted]) VALUES (12, N'MIke', N'mikel', N'kk@jj.be', 1, N'-4', N'lo', N'lo', NULL, 0)
GO
INSERT [dbo].[Membre] ([idMembre], [Nom], [Prenom], [Email], [Pays], [Telephone], [Login], [Password], [PhotoUser], [isDeleted]) VALUES (13, N'zorro', N'mikel', N'fd', 1, N'-45', N'lop', N'oeDpVa9JpZtpxa4Jp7+VSlBHzmRuuDokaKclx9PcGhY=', NULL, 0)
GO
INSERT [dbo].[Membre] ([idMembre], [Nom], [Prenom], [Email], [Pays], [Telephone], [Login], [Password], [PhotoUser], [isDeleted]) VALUES (14, N'MIke', N'dqs', N'fd', -1, N'-45', N'de', N'de', NULL, 0)
GO
INSERT [dbo].[Membre] ([idMembre], [Nom], [Prenom], [Email], [Pays], [Telephone], [Login], [Password], [PhotoUser], [isDeleted]) VALUES (15, N'zz', N'zz', N'zz@ff.be', 1, N'45', N'dd', N'ddd', NULL, 0)
GO
INSERT [dbo].[Membre] ([idMembre], [Nom], [Prenom], [Email], [Pays], [Telephone], [Login], [Password], [PhotoUser], [isDeleted]) VALUES (16, N'Person', N'Mike', N'michael.Person@cognitic.be', 0, N'0032498', N'Mike@bel.be', N'test', NULL, 0)
GO
INSERT [dbo].[Membre] ([idMembre], [Nom], [Prenom], [Email], [Pays], [Telephone], [Login], [Password], [PhotoUser], [isDeleted]) VALUES (17, N'mmm', N'mmm', N'mm@mm.be', 1, N'04984454', N'mmm', N'ttt', NULL, 0)
GO
SET IDENTITY_INSERT [dbo].[Membre] OFF
GO
INSERT [dbo].[MembreBienEchange] ([idMembre], [idBien], [DateDebEchange], [DateFinEchange], [Assurance], [Valide]) VALUES (1, 3, CAST(N'2016-08-14' AS Date), CAST(N'2016-08-18' AS Date), 0, 1)
GO
INSERT [dbo].[MembreBienEchange] ([idMembre], [idBien], [DateDebEchange], [DateFinEchange], [Assurance], [Valide]) VALUES (1, 3, CAST(N'2017-07-16' AS Date), CAST(N'2017-07-28' AS Date), 0, 1)
GO
INSERT [dbo].[MembreBienEchange] ([idMembre], [idBien], [DateDebEchange], [DateFinEchange], [Assurance], [Valide]) VALUES (3, 2, CAST(N'2017-07-16' AS Date), CAST(N'2017-07-28' AS Date), 0, 1)
GO
SET IDENTITY_INSERT [dbo].[Options] ON 
GO
INSERT [dbo].[Options] ([idOption], [Libelle]) VALUES (1, N'Chien admis')
GO
INSERT [dbo].[Options] ([idOption], [Libelle]) VALUES (2, N'Lave Linge')
GO
INSERT [dbo].[Options] ([idOption], [Libelle]) VALUES (3, N'Lave vaisselle')
GO
INSERT [dbo].[Options] ([idOption], [Libelle]) VALUES (4, N'Wifi')
GO
INSERT [dbo].[Options] ([idOption], [Libelle]) VALUES (5, N'Parking')
GO
INSERT [dbo].[Options] ([idOption], [Libelle]) VALUES (6, N'Piscine')
GO
INSERT [dbo].[Options] ([idOption], [Libelle]) VALUES (7, N'Feu ouvert')
GO
INSERT [dbo].[Options] ([idOption], [Libelle]) VALUES (8, N'Lit enfant')
GO
INSERT [dbo].[Options] ([idOption], [Libelle]) VALUES (9, N'WC')
GO
INSERT [dbo].[Options] ([idOption], [Libelle]) VALUES (10, N'Salle de bain')
GO
INSERT [dbo].[Options] ([idOption], [Libelle]) VALUES (11, N'Jardin')
GO
INSERT [dbo].[Options] ([idOption], [Libelle]) VALUES (12, N'Douche')
GO
SET IDENTITY_INSERT [dbo].[Options] OFF
GO
INSERT [dbo].[OptionsBien] ([idOption], [idBien], [Valeur]) VALUES (1, 2, N'Oui')
GO
INSERT [dbo].[OptionsBien] ([idOption], [idBien], [Valeur]) VALUES (1, 3, N'Non')
GO
INSERT [dbo].[OptionsBien] ([idOption], [idBien], [Valeur]) VALUES (2, 2, N'Non')
GO
INSERT [dbo].[OptionsBien] ([idOption], [idBien], [Valeur]) VALUES (2, 18, N'Non')
GO
INSERT [dbo].[OptionsBien] ([idOption], [idBien], [Valeur]) VALUES (3, 3, N'Non')
GO
INSERT [dbo].[OptionsBien] ([idOption], [idBien], [Valeur]) VALUES (4, 2, N'Oui')
GO
INSERT [dbo].[OptionsBien] ([idOption], [idBien], [Valeur]) VALUES (6, 2, N'Oui')
GO
INSERT [dbo].[OptionsBien] ([idOption], [idBien], [Valeur]) VALUES (8, 3, N'1')
GO
INSERT [dbo].[OptionsBien] ([idOption], [idBien], [Valeur]) VALUES (8, 18, N'1')
GO
INSERT [dbo].[OptionsBien] ([idOption], [idBien], [Valeur]) VALUES (9, 2, N'1')
GO
INSERT [dbo].[OptionsBien] ([idOption], [idBien], [Valeur]) VALUES (10, 2, N'5')
GO
SET IDENTITY_INSERT [dbo].[Pays] ON 
GO
INSERT [dbo].[Pays] ([idPays], [Libelle], [Photo]) VALUES (1, N'Belgique', NULL)
GO
INSERT [dbo].[Pays] ([idPays], [Libelle], [Photo]) VALUES (2, N'France', NULL)
GO
INSERT [dbo].[Pays] ([idPays], [Libelle], [Photo]) VALUES (3, N'Italie', NULL)
GO
INSERT [dbo].[Pays] ([idPays], [Libelle], [Photo]) VALUES (4, N'Pays-Bas', NULL)
GO
INSERT [dbo].[Pays] ([idPays], [Libelle], [Photo]) VALUES (5, N'Luxembourg', NULL)
GO
INSERT [dbo].[Pays] ([idPays], [Libelle], [Photo]) VALUES (6, N'Austalie', NULL)
GO
INSERT [dbo].[Pays] ([idPays], [Libelle], [Photo]) VALUES (7, N'USA', NULL)
GO
INSERT [dbo].[Pays] ([idPays], [Libelle], [Photo]) VALUES (8, N'Anglettere', NULL)
GO
INSERT [dbo].[Pays] ([idPays], [Libelle], [Photo]) VALUES (9, N'Espagne', NULL)
GO
INSERT [dbo].[Pays] ([idPays], [Libelle], [Photo]) VALUES (10, N'Portugal', NULL)
GO
INSERT [dbo].[Pays] ([idPays], [Libelle], [Photo]) VALUES (11, N'Islande', NULL)
GO
SET IDENTITY_INSERT [dbo].[Pays] OFF
GO
SET IDENTITY_INSERT [dbo].[User] ON 
GO
INSERT [dbo].[User] ([idUser], [login], [password]) VALUES (1, N'Mike', N'test')
GO
SET IDENTITY_INSERT [dbo].[User] OFF
GO
ALTER TABLE [dbo].[AvisMembreBien] ADD  CONSTRAINT [DF_AvisMembreBien_Approuve]  DEFAULT ((0)) FOR [Approuve]
GO
ALTER TABLE [dbo].[BienEchange] ADD  CONSTRAINT [DF_BienEchange_Pays]  DEFAULT ((1)) FOR [Pays]
GO
ALTER TABLE [dbo].[BienEchange] ADD  CONSTRAINT [DF_BienEchange_AssuranceObligatoire]  DEFAULT ((0)) FOR [AssuranceObligatoire]
GO
ALTER TABLE [dbo].[BienEchange] ADD  CONSTRAINT [DF_BienEchange_isEnabled]  DEFAULT ((0)) FOR [isEnabled]
GO
ALTER TABLE [dbo].[BienEchange] ADD  CONSTRAINT [DF_BienEchange_DateCreation]  DEFAULT (getdate()) FOR [DateCreation]
GO
ALTER TABLE [dbo].[CaracteristiqueBien] ADD  CONSTRAINT [DF_CaracteristiqueBien_fk_idCaracteristique]  DEFAULT ((0)) FOR [fk_idCaracteristique]
GO
ALTER TABLE [dbo].[CaracteristiqueBien] ADD  CONSTRAINT [DF_CaracteristiqueBien_fk_idBien]  DEFAULT ((0)) FOR [fk_idBien]
GO
ALTER TABLE [dbo].[Membre] ADD  CONSTRAINT [DF_Membre_isDeleted]  DEFAULT ((0)) FOR [isDeleted]
GO
ALTER TABLE [dbo].[MembreBienEchange] ADD  CONSTRAINT [DF_MembreBienEchange_Valide]  DEFAULT ((0)) FOR [Valide]
GO
ALTER TABLE [dbo].[Avis]  WITH CHECK ADD  CONSTRAINT [FK_Avis_Bien] FOREIGN KEY([fk_Bien])
REFERENCES [dbo].[Bien] ([idBien])
GO
ALTER TABLE [dbo].[Avis] CHECK CONSTRAINT [FK_Avis_Bien]
GO
ALTER TABLE [dbo].[Avis]  WITH CHECK ADD  CONSTRAINT [FK_Avis_Membre] FOREIGN KEY([fk_Membre])
REFERENCES [dbo].[Membre] ([idMembre])
GO
ALTER TABLE [dbo].[Avis] CHECK CONSTRAINT [FK_Avis_Membre]
GO
ALTER TABLE [dbo].[AvisMembreBien]  WITH CHECK ADD  CONSTRAINT [FK_AvisMembreBien_BienEchange] FOREIGN KEY([idBien])
REFERENCES [dbo].[BienEchange] ([idBien])
GO
ALTER TABLE [dbo].[AvisMembreBien] CHECK CONSTRAINT [FK_AvisMembreBien_BienEchange]
GO
ALTER TABLE [dbo].[AvisMembreBien]  WITH CHECK ADD  CONSTRAINT [FK_AvisMembreBien_membre] FOREIGN KEY([idMembre])
REFERENCES [dbo].[Membre] ([idMembre])
GO
ALTER TABLE [dbo].[AvisMembreBien] CHECK CONSTRAINT [FK_AvisMembreBien_membre]
GO
ALTER TABLE [dbo].[Bien]  WITH CHECK ADD  CONSTRAINT [FK_Bien_Membre] FOREIGN KEY([fk_Membre])
REFERENCES [dbo].[Membre] ([idMembre])
GO
ALTER TABLE [dbo].[Bien] CHECK CONSTRAINT [FK_Bien_Membre]
GO
ALTER TABLE [dbo].[Bien]  WITH CHECK ADD  CONSTRAINT [FK_Bien_Pays] FOREIGN KEY([fk_Pays])
REFERENCES [dbo].[Pays] ([idPays])
GO
ALTER TABLE [dbo].[Bien] CHECK CONSTRAINT [FK_Bien_Pays]
GO
ALTER TABLE [dbo].[BienEchange]  WITH CHECK ADD  CONSTRAINT [FK_BienEchange_membre] FOREIGN KEY([idMembre])
REFERENCES [dbo].[Membre] ([idMembre])
GO
ALTER TABLE [dbo].[BienEchange] CHECK CONSTRAINT [FK_BienEchange_membre]
GO
ALTER TABLE [dbo].[BienEchange]  WITH CHECK ADD  CONSTRAINT [FK_BienEchange_Pays] FOREIGN KEY([Pays])
REFERENCES [dbo].[Pays] ([idPays])
GO
ALTER TABLE [dbo].[BienEchange] CHECK CONSTRAINT [FK_BienEchange_Pays]
GO
ALTER TABLE [dbo].[CaracteristiqueBien]  WITH CHECK ADD  CONSTRAINT [FK_CaracteristiqueBien_Bien] FOREIGN KEY([fk_idBien])
REFERENCES [dbo].[Bien] ([idBien])
GO
ALTER TABLE [dbo].[CaracteristiqueBien] CHECK CONSTRAINT [FK_CaracteristiqueBien_Bien]
GO
ALTER TABLE [dbo].[CaracteristiqueBien]  WITH CHECK ADD  CONSTRAINT [FK_CaracteristiqueBien_Caracteristique] FOREIGN KEY([fk_idCaracteristique])
REFERENCES [dbo].[Caracteristique] ([idCaractèristique])
GO
ALTER TABLE [dbo].[CaracteristiqueBien] CHECK CONSTRAINT [FK_CaracteristiqueBien_Caracteristique]
GO
ALTER TABLE [dbo].[Echange]  WITH CHECK ADD  CONSTRAINT [FK_Echange_Bien] FOREIGN KEY([fk_Bien])
REFERENCES [dbo].[Bien] ([idBien])
GO
ALTER TABLE [dbo].[Echange] CHECK CONSTRAINT [FK_Echange_Bien]
GO
ALTER TABLE [dbo].[Echange]  WITH CHECK ADD  CONSTRAINT [FK_Echange_Membre] FOREIGN KEY([fk_Membre])
REFERENCES [dbo].[Membre] ([idMembre])
GO
ALTER TABLE [dbo].[Echange] CHECK CONSTRAINT [FK_Echange_Membre]
GO
ALTER TABLE [dbo].[MembreBienEchange]  WITH CHECK ADD  CONSTRAINT [FK_MembreBienEchange_BienEchange] FOREIGN KEY([idBien])
REFERENCES [dbo].[BienEchange] ([idBien])
GO
ALTER TABLE [dbo].[MembreBienEchange] CHECK CONSTRAINT [FK_MembreBienEchange_BienEchange]
GO
ALTER TABLE [dbo].[MembreBienEchange]  WITH CHECK ADD  CONSTRAINT [FK_MembreBienEchange_membre] FOREIGN KEY([idMembre])
REFERENCES [dbo].[Membre] ([idMembre])
GO
ALTER TABLE [dbo].[MembreBienEchange] CHECK CONSTRAINT [FK_MembreBienEchange_membre]
GO
ALTER TABLE [dbo].[OptionsBien]  WITH CHECK ADD  CONSTRAINT [FK_OptionsBien_BienEchange] FOREIGN KEY([idBien])
REFERENCES [dbo].[BienEchange] ([idBien])
GO
ALTER TABLE [dbo].[OptionsBien] CHECK CONSTRAINT [FK_OptionsBien_BienEchange]
GO
ALTER TABLE [dbo].[OptionsBien]  WITH CHECK ADD  CONSTRAINT [FK_OptionsBien_Options] FOREIGN KEY([idOption])
REFERENCES [dbo].[Options] ([idOption])
GO
ALTER TABLE [dbo].[OptionsBien] CHECK CONSTRAINT [FK_OptionsBien_Options]
GO


CREATE LOGIN Administrator WITH PASSWORD=N'Test1234=', 
DEFAULT_DATABASE=[HolidayRental], DEFAULT_LANGUAGE=[us_english],
CHECK_EXPIRATION=OFF, CHECK_POLICY=OFF
GO

CREATE LOGIN Visitor WITH PASSWORD=N'Test1234=', 
DEFAULT_DATABASE=[HolidayRental], DEFAULT_LANGUAGE=[us_english],
CHECK_EXPIRATION=OFF, CHECK_POLICY=OFF
GO


Use [HolidayRental]
CREATE USER Administrator FOR LOGIN Administrator
GO 
ALTER ROLE [db_owner] ADD MEMBER Administrator

CREATE USER Visitor FOR LOGIN Visitor
GO 
ALTER ROLE [db_datareader] ADD MEMBER Visitor
GO 
ALTER ROLE [db_datawriter] ADD MEMBER Visitor